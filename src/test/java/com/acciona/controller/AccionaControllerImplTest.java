package com.acciona.controller;

import com.acciona.domain.dto.HashtagDto;
import com.acciona.domain.dto.TweetDto;
import com.acciona.service.AccionaServiceImpl;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@AutoConfigureMockMvc
@SpringBootTest
class AccionaControllerImplTest {

    @MockBean
    private AccionaServiceImpl accionaServiceImpl;

    @Autowired
    private AccionaControllerImpl accionaControllerImpl;

    @Autowired
    private MockMvc mockMvc;


    @BeforeClass
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        accionaControllerImpl = new AccionaControllerImpl(accionaServiceImpl);
        mockMvc = MockMvcBuilders.standaloneSetup(accionaServiceImpl).build();
    }

    @Test
    void getAllTweet() throws Exception {
        TweetDto[] tweetDtosTest = {new TweetDto("1", "user1", "text1", "location1", false), new TweetDto("2", "user1", "text2", "location2", false)};
        BDDMockito.given(accionaServiceImpl.getAllTweet()).willReturn(tweetDtosTest);
        RequestBuilder request =
                MockMvcRequestBuilders.get("http://localhost:8080/api/tweets");

        MvcResult result = mockMvc.perform(request).andReturn();
        String tweetDtosString = "" + Arrays.toString(tweetDtosTest).replaceAll("TweetDto\\(", "{").replaceAll("\\)", "}").replaceAll(" ", "").replaceAll("=", ":");
        assertEquals(result.getResponse().getContentAsString().replaceAll("\"", ""), tweetDtosString);
    }


    @Test
    void validateTweet() throws Exception {

        TweetDto tweetDtoTest = new TweetDto("1", "user1", "text1", "location1", true);

        BDDMockito.given(accionaServiceImpl.validateTweet("1")).willReturn(tweetDtoTest);
        RequestBuilder request =
                MockMvcRequestBuilders.put("http://localhost:8080/api/tweets/1/validate");
        MvcResult result = mockMvc.perform(request).andReturn();
        String tweetDtosString = "" + tweetDtoTest.toString().replaceAll("TweetDto\\(", "{").replaceAll("\\)", "}").replaceAll(" ", "").replaceAll("=", ":");
        assertEquals(result.getResponse().getContentAsString().replaceAll("\"", ""), tweetDtosString);
    }

    @Test
    void getValidatedTweetByUser() throws Exception {
        TweetDto[] tweetDtosTest = {new TweetDto("1", "user1", "text1", "location1", true), new TweetDto("2", "user1", "text2", "location2", true)};
        BDDMockito.given(accionaServiceImpl.getValidatedTweetByUser("user1")).willReturn(tweetDtosTest);
        RequestBuilder request =
                MockMvcRequestBuilders.get("http://localhost:8080/api/tweets/users/user1/tweets-validated");
        MvcResult result = mockMvc.perform(request).andReturn();
        String tweetDtosString = "" + Arrays.toString(tweetDtosTest).replaceAll("TweetDto\\(", "{").replaceAll("\\)", "}").replaceAll(" ", "").replaceAll("=", ":");
        assertEquals(result.getResponse().getContentAsString().replaceAll("\"", ""), tweetDtosString);
    }

    @Test
    void getTopHastags() throws Exception {
        List<HashtagDto> hashtagDto  =new ArrayList<HashtagDto>();
        hashtagDto.add(new HashtagDto("3","#test3",3));
        hashtagDto.add(new HashtagDto("2","#test2",2));
        BDDMockito.given(accionaServiceImpl.getTopHashtags()).willReturn(hashtagDto.toArray(new HashtagDto[0]));
        RequestBuilder request =
                MockMvcRequestBuilders.get("http://localhost:8080/api/hashtags/top");
        MvcResult result = mockMvc.perform(request).andReturn();
        String hashtagDtoString=""+hashtagDto.toString().replaceAll("HashtagDto\\(", "{").replaceAll("\\)", "}").replaceAll(" ", "").replaceAll("=", ":");
        assertEquals(result.getResponse().getContentAsString().replaceAll("\"", ""), hashtagDtoString);
    }
}