package com.acciona.service;

import com.acciona.domain.dto.HashtagDto;
import com.acciona.domain.dto.TweetDto;
import com.acciona.domain.entity.Hashtag;
import com.acciona.domain.entity.Tweet;
import com.acciona.repository.HashtagReactiveRepository;
import com.acciona.repository.TweetReactiveRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class AccionaServiceImplTest {

    @Autowired
    private AccionaServiceImpl accionaServiceImpl;

    @MockBean
    private TweetReactiveRepository tweetReactiveRepository;

    @MockBean
    private HashtagReactiveRepository hashtagReactiveRepository;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        accionaServiceImpl = new AccionaServiceImpl(tweetReactiveRepository, hashtagReactiveRepository);
    }


    @Test
    void getAllTweet() {
        List<Tweet> tweetTest = new ArrayList<Tweet>();
        tweetTest.add(new Tweet("user1", "text1", "location1", false));
        tweetTest.add(new Tweet("user2", "text2", "location2", true));
        tweetTest.add(new Tweet("user3", "text3", "location3", false));

        Flux<Tweet> tweetFlux = Flux.fromStream(tweetTest.stream());
        BDDMockito.given(tweetReactiveRepository.findAll()).willReturn(tweetFlux);
        List<TweetDto> tweetDtoGetted = Arrays.asList(accionaServiceImpl.getAllTweet());
        assertEquals(tweetDtoGetted.toString().replaceAll("TweetDto", ""), tweetTest.toString().replaceAll("Tweet", ""));

    }

    @Test
    void validateTweet() {
        Tweet tweetTest = new Tweet("user1", "text1", "location1", false);
        tweetTest.setId("1");
        Mono<Tweet> tweetMono = Mono.just(tweetTest);

        Mono<Boolean> booleanMonoFalse = Mono.just(false);
        BDDMockito.given(tweetReactiveRepository.existsById("1")).willReturn(booleanMonoFalse);
        TweetDto tweetDtoGetted1 = accionaServiceImpl.validateTweet("1");
        assertEquals(tweetDtoGetted1,new TweetDto());

        Mono<Boolean> booleanMonoTrue = Mono.just(true);
        BDDMockito.given(tweetReactiveRepository.existsById("2")).willReturn(booleanMonoTrue);
        BDDMockito.given(tweetReactiveRepository.findById("2")).willReturn(tweetMono);
        BDDMockito.given(tweetReactiveRepository.save(tweetTest)).willReturn(tweetMono);
        TweetDto tweetDtoGetted2 = accionaServiceImpl.validateTweet("2");
        assertEquals(tweetDtoGetted2.toString().replaceAll("TweetDto", ""), tweetTest.toString().replaceAll("Tweet", "").replaceAll("false", "true"));

    }

    @Test
    void getValidatedTweetByUser() {
        List<Tweet> tweetTest = new ArrayList<Tweet>();
        tweetTest.add(new Tweet("user2", "text1", "location1", true));
        tweetTest.add(new Tweet("user2", "text2", "location2", true));
        Flux<Tweet> tweetFlux = Flux.fromStream(tweetTest.stream());

        Mono<Boolean> booleanMonoFalse = Mono.just(false);
        BDDMockito.given(tweetReactiveRepository.existsByUserAndValidation("user1", true)).willReturn(booleanMonoFalse);
        List<TweetDto> tweetDtoGetted1 = Arrays.asList(accionaServiceImpl.getValidatedTweetByUser("user1"));
        assertEquals(tweetDtoGetted1.toString(),"[]");

        Mono<Boolean> booleanMonoTrue = Mono.just(true);
        BDDMockito.given(tweetReactiveRepository.existsByUserAndValidation("user2", true)).willReturn(booleanMonoTrue);
        BDDMockito.given(tweetReactiveRepository.findAllByUserAndValidation("user2", true)).willReturn(tweetFlux);
        List<TweetDto> tweetDtoGetted2 = Arrays.asList(accionaServiceImpl.getValidatedTweetByUser("user2"));
        assertEquals(tweetDtoGetted2.toString().replaceAll("TweetDto", ""), tweetTest.toString().replaceAll("Tweet", ""));
    }


    @Test
    void getTopHashtags() {

        List<Hashtag> hashtagList = new ArrayList<Hashtag>();
        hashtagList.add(new Hashtag("#hashtag1", 1));
        hashtagList.add(new Hashtag("#hashtag2", 2));
        hashtagList.add(new Hashtag("#hashtag3", 3));
        Flux<Hashtag> hashtagFLux = Flux.fromArray(hashtagList.toArray(new Hashtag[0]));
        BDDMockito.given(hashtagReactiveRepository.findAll()).willReturn(hashtagFLux);
        assertTrue(Arrays.toString(accionaServiceImpl.getTopHashtags()).contains(hashtagList.get(2).toString().replaceAll("Hashtag\\(id=null, ","")));
        assertTrue(Arrays.toString(accionaServiceImpl.getTopHashtags()).contains(hashtagList.get(1).toString().replaceAll("Hashtag\\(id=null, ","")));

        Flux<Hashtag> hashtagFLux2 = Flux.fromArray(new Hashtag[0]);
        BDDMockito.given(hashtagReactiveRepository.findAll()).willReturn(hashtagFLux2);
        HashtagDto hashtagDtoListError[] = {new HashtagDto("error", "NO HAY SUFICIENTES HASHTAGS PARA UN TOP " + 2, 0)};
        assertEquals(Arrays.toString(accionaServiceImpl.getTopHashtags()),Arrays.toString(hashtagDtoListError));
    }


    @Test
    void tweetToTweetDto() {
        Tweet tweetTest = new Tweet("user1", "text1", "location1", true);
        tweetTest.setId("1");
        assertEquals(accionaServiceImpl.tweetToTweetDto(tweetTest).toString(), tweetTest.toString().replaceAll("Tweet", "TweetDto"));
    }

    @Test
    void hashtagToHashtagDto() {
        Hashtag hashtagTest = new Hashtag("hashtag1", 1);
        hashtagTest.setId("1");
        assertEquals(accionaServiceImpl.hastagToHashtagDto(hashtagTest).toString(), hashtagTest.toString().replaceAll("Hashtag", "HashtagDto"));
    }

    @Test
    void saveTweet() {
        Tweet tweetTest = new Tweet("user1", "text1", "location1", false);
        BDDMockito.given(tweetReactiveRepository.save(tweetTest)).willReturn(Mono.just(tweetTest));
        assertEquals(accionaServiceImpl.saveTweet("user1","text1","location1").block().toString(), tweetTest.toString());
    }

    @Test
    void saveHashtag() {
        List<String> hashtagStringList = new ArrayList<String>();
        Hashtag hashtagTest1 = new Hashtag("hashtag1", 1);
        hashtagTest1.setId("1");
        hashtagStringList.add(hashtagTest1.getName());

        BDDMockito.given(hashtagReactiveRepository.findByName(hashtagTest1.getName())).willReturn(Mono.just(hashtagTest1));
        BDDMockito.given(hashtagReactiveRepository.save(hashtagTest1)).willReturn(Mono.just(hashtagTest1));
        assertEquals(accionaServiceImpl.saveHashtag(hashtagStringList).toString(), Arrays.asList(hashtagTest1).toString());

     }


}
