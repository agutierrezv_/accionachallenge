package com.acciona.repository;

import com.acciona.domain.entity.Tweet;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Repository
public interface TweetReactiveRepository extends ReactiveMongoRepository<Tweet,String> {

    Mono<Boolean> existsByUserAndValidation(String user, boolean validation);

    Flux<Tweet> findAllByUserAndValidation(String user, boolean validation);
}
