package com.acciona.repository;

import com.acciona.domain.entity.Hashtag;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface HashtagReactiveRepository extends ReactiveMongoRepository<Hashtag,String> {

    Mono<Hashtag> findByName(String name);

}

