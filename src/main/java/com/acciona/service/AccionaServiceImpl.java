package com.acciona.service;

import com.acciona.domain.dto.HashtagDto;
import com.acciona.domain.dto.TweetDto;
import com.acciona.domain.entity.Hashtag;
import com.acciona.domain.entity.Tweet;
import com.acciona.repository.HashtagReactiveRepository;
import com.acciona.repository.TweetReactiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AccionaServiceImpl implements AccionaService {


    @Autowired
    private final TweetReactiveRepository tweetReactiveRepository;

    @Autowired
    private final HashtagReactiveRepository hashtagReactiveRepository;


    @Autowired
    public AccionaServiceImpl(TweetReactiveRepository tweetReactiveRepository, HashtagReactiveRepository hashtagReactiveRepository) {
        this.tweetReactiveRepository = tweetReactiveRepository;
        this.hashtagReactiveRepository = hashtagReactiveRepository;
    }

    @Value("${numHashtags}")
    private Integer numHashtags;

    public TweetDto tweetToTweetDto(Tweet tweet) {
        return new TweetDto(tweet.getId(), tweet.getUser(), tweet.getText(), tweet.getLocation(), tweet.isValidation());
    }

    public HashtagDto hastagToHashtagDto(Hashtag hastag) {
        return new HashtagDto(hastag.getId(), hastag.getName(), hastag.getCount());
    }

    @Override
    public TweetDto[] getAllTweet() {
        Flux<Tweet> tweets = tweetReactiveRepository.findAll();
        ArrayList<TweetDto> tweetDto = new ArrayList<TweetDto>();
        tweets.collectList().block().stream().forEach(tweet -> tweetDto.add(this.tweetToTweetDto(tweet)));
        return tweetDto.toArray(new TweetDto[0]);
    }


    @Override
    public TweetDto validateTweet(String idToValidate) {
        if (!tweetReactiveRepository.existsById(idToValidate).block()) {
            return new TweetDto();
        }
        Tweet tweetGetted = tweetReactiveRepository.findById(idToValidate).block();
        tweetGetted.setValidation(true);
        tweetReactiveRepository.save(tweetGetted).block();
        return this.tweetToTweetDto(tweetGetted);
    }

    @Override
    public TweetDto[] getValidatedTweetByUser(String user) {
        if (!tweetReactiveRepository.existsByUserAndValidation(user, true).block()) {
            return new TweetDto[0];
        }
        List<Tweet> tweets = tweetReactiveRepository.findAllByUserAndValidation(user, true).collectList().block();
        ArrayList<TweetDto> tweetDto = new ArrayList<TweetDto>();
        tweets.stream().forEach(tweet -> tweetDto.add(this.tweetToTweetDto(tweet)));
        return tweetDto.toArray(new TweetDto[0]);
    }

    @Override
    public HashtagDto[] getTopHashtags() {

        int top = this.numHashtags != null ? this.numHashtags : 2;
        List<Hashtag> hashtagGettedList = hashtagReactiveRepository.findAll().collectList().block();

        if (hashtagGettedList.isEmpty()||hashtagGettedList.size()<top) {
            HashtagDto hashtagDto[] = new HashtagDto[1];
            hashtagDto[0] = new HashtagDto("error", "NO HAY SUFICIENTES HASHTAGS PARA UN TOP " + top, 0);
            return hashtagDto;
        }

        List<Hashtag> hashtagSubSortList = hashtagGettedList.stream().sorted(Comparator.comparing(Hashtag::getCount).reversed()).collect(Collectors.toList()).subList(0, top);
        List<HashtagDto> hashtagDtoList = new ArrayList<HashtagDto>();
        hashtagSubSortList.stream().forEach(hashtag -> hashtagDtoList.add(hastagToHashtagDto(hashtag)));

        return hashtagDtoList.toArray(new HashtagDto[0]);


    }


    @Override
    public Mono<Tweet> saveTweet(String user, String text, String location) {

        return tweetReactiveRepository.save(new Tweet(user, text, location, false));
    }


    @Override
    public List<Hashtag> saveHashtag(List<String> hashtagList) {
        List<Hashtag> hashtagsSaved= new ArrayList<Hashtag>();
        hashtagList.stream().forEach(hashtag -> {
            Hashtag hashtagGetted = hashtagReactiveRepository.findByName(hashtag).block();
            if (hashtagGetted != null) {
                hashtagGetted.setCount(hashtagGetted.getCount() + 1);
                hashtagsSaved.add(hashtagReactiveRepository.save(hashtagGetted).block());
            } else {
                hashtagsSaved.add(hashtagReactiveRepository.save(new Hashtag(hashtag, 1)).block());
            }
        });
        return hashtagsSaved;
    }



}
