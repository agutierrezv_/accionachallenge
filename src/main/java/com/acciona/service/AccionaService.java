package com.acciona.service;

import com.acciona.domain.dto.HashtagDto;
import com.acciona.domain.dto.TweetDto;
import com.acciona.domain.entity.Hashtag;
import com.acciona.domain.entity.Tweet;
import reactor.core.publisher.Mono;


import java.util.List;


public interface AccionaService {

    TweetDto[] getAllTweet();

    TweetDto validateTweet(String idToValidate);

    TweetDto[] getValidatedTweetByUser(String user);

    HashtagDto[] getTopHashtags();

    Mono<Tweet> saveTweet(String user, String text, String location);

    List<Hashtag> saveHashtag(List<String> hashtags);
}
