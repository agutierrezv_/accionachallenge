package com.acciona.controller;

import com.acciona.domain.dto.HashtagDto;
import com.acciona.domain.dto.TweetDto;
import com.acciona.service.AccionaService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class AccionaControllerImpl implements AccionaController {

    private final AccionaService accionaService;

    @Autowired
    public AccionaControllerImpl(AccionaService accionaService) {
        this.accionaService = accionaService;
    }

    @ApiOperation(value = "Devuelve una lista de objetos TweetsDto", notes = "Comprueba la BD y devuelve una lista con los objectos TweetDto que se encuentran en ella en ese momento.")
    @Override
    @GetMapping("/tweets")
    public ResponseEntity<TweetDto[]> getAllTweet() {
        return ResponseEntity.ok(accionaService.getAllTweet());
    }

    @ApiOperation(value = "Valida un tweet por su id", notes = "Comprueba y devuelve en la BD un tweet por su id, lo valida y lo vuelve a guardar en esta.")
    @Override
    @PutMapping("/tweets/{id}/validate")
    public ResponseEntity<TweetDto> validateTweet(@PathVariable String id) {
        return ResponseEntity.ok(accionaService.validateTweet(id));
    }

    @ApiOperation(value = "Devuelve todos los tweets validados de un usuario", notes = "Comprueba y devuelve de la BD todos los tweets validados de un usuario.")
    @Override
    @GetMapping("/tweets/users/{user}/tweets-validated")
    public ResponseEntity<TweetDto[]> getValidatedTweetByUser(@PathVariable String user) {
        return ResponseEntity.ok(accionaService.getValidatedTweetByUser(user));
    }

    @ApiOperation(value = "Devuelve el top x con los hastags mas usados", notes = "Comprueba y devuelve en la BD un top con los x hastags más usados en los textos de los objetos Tweet siendo X una variable de entorno entera.")
    @Override
    @GetMapping("/hashtags/top")
    public ResponseEntity<HashtagDto[]> getTopHashtags() {
        return ResponseEntity.ok(accionaService.getTopHashtags());
    }
}
