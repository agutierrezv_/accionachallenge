package com.acciona.controller;

import com.acciona.domain.dto.HashtagDto;
import com.acciona.domain.dto.TweetDto;
import org.springframework.http.ResponseEntity;


public interface AccionaController {

    ResponseEntity<TweetDto[]> getAllTweet();

    ResponseEntity<TweetDto> validateTweet(String id);

    ResponseEntity<TweetDto[]> getValidatedTweetByUser(String user);

    ResponseEntity<HashtagDto[]> getTopHashtags();
}
