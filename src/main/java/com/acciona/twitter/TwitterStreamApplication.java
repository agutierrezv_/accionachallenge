package com.acciona.twitter;

import com.acciona.service.AccionaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Component
public class TwitterStreamApplication implements ApplicationListener<ApplicationReadyEvent> {

    private final AccionaService accionaService;

    @Autowired
    public TwitterStreamApplication(AccionaService accionaService) {
        this.accionaService = accionaService;
    }

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        this.comenzarStreamApi();
        return;
    }

    @Value("${oauth.consumerKey}")
    private String consumerKey;
    @Value("${oauth.consumerSecret}")
    private String consumerSecret;
    @Value("${oauth.accessToken}")
    private String accessToken;
    @Value("${oauth.accessTokenSecret}")
    private String accessTokenSecret;
    @Value("${numFollowers}")
    private int numFollowers;
    @Value("${languages}")
    private List<String> languages;


    public void comenzarStreamApi() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(consumerKey)
                .setOAuthConsumerSecret(consumerSecret)
                .setOAuthAccessToken(accessToken)
                .setOAuthAccessTokenSecret(accessTokenSecret);
        TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();

        Flux<Status> tweets = Flux.create(sink -> {
            twitterStream.onStatus(sink::next);
            twitterStream.onException(sink::error);
            sink.onCancel(twitterStream::shutdown);
            twitterStream.sample();

        });

        tweets.filter(status ->  status.getUser().getFollowersCount() > numFollowers && languages.contains(status.getLang()))
                .flatMap(status -> {
                   saveHashtags(status);
                   return accionaService.saveTweet(status.getUser().getScreenName(), status.getText(), status.getUser().getLocation());
                }).subscribe();
        }

        private void saveHashtags(Status status){
            List<String> hashtagList = new ArrayList<>();
                    Arrays.stream(status.getHashtagEntities()).forEach(hashtagEntity -> {
                        hashtagList.add(hashtagEntity.getText().toLowerCase());
                    });
                    if (!hashtagList.isEmpty()) {
                        accionaService.saveHashtag(hashtagList);
                    }

        }


}
