package com.acciona.domain.entity;


import lombok.Data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection="Hashtag")
public class Hashtag {

    @Id
    private String id;
    private String name;
    private int count;

    public Hashtag( String name, int count){
        this.name=name;
        this.count=count;
    }
    

}
