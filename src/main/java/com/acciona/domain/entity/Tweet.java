package com.acciona.domain.entity;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;



@Data
@Document(collection="Tweets")
public class Tweet {

    @Id
    private String id;
    private String user;
    private String text;
    private String location;
    private boolean validation;


    public Tweet( String user, String text, String location,boolean validation){

        this.user=user;
        this.text=text;
        this.location=location;
        this.validation=validation;
    }

}
