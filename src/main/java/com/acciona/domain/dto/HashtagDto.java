package com.acciona.domain.dto;

import lombok.Data;



@Data
public class HashtagDto {


    private String id;
    private String name;
    private int count;

    public HashtagDto(String id, String name, int count){
        this.id=id;
        this.name=name;
        this.count=count;
    }
}