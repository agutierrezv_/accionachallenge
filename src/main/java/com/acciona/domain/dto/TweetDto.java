package com.acciona.domain.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TweetDto {

    private String id;
    private String user;
    private String text;
    private String location;
    private boolean validation;

    public TweetDto(String id, String user, String text, String location,boolean validation){
        this.id=id;
        this.user=user;
        this.text=text;
        this.location=location;
        this.validation=validation;
    }

}
