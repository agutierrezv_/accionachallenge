# AccionaChallenge

En primer lugar será necesario acceder al archivo application.yml (/src/main/resources/application.yml) y complete con sus tokens de desarrollador de twitter los valores de las variables de entorno: consumerKey, consumerSecret, accessToken y accessTokenSecret. También desde este archivo se podrán modificar los valores por defecto de filtrado de tweets (idiomas y número de seguidores).

Empaquete la aplicación en un nuevo .jar utilizando el comando "mvn clean package -DskipTests=true" desde la raíz del proyecto.

Si desea ejecutar la aplicación en un contenedor de docker utilice el comando "docker-compose up --build" desde la raíz del proyecto (donde se encuentra el archivo docker-compose.yml)

Una vez hecho esto:
El módulo acciona contiene la API de gestión de los tweets que se pidió para la prueba.

La documentación del módulo se encuentra disponible una vez arrancado el docker o el jar en la direccion:
Documentación API acciona: http://localhost:8080/swagger-ui/index.html#/


